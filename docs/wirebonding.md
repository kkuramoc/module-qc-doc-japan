# ワイヤボンディング手順

Last-Update: 2023-06-06

基本的にREPICの方に依頼する．

## Pre-productionでの手順

!!! note
    REPIC側との間では，PCBの右上のフレームに書いてある下４ケタのID（山下ID）の書いてあるラベルのついたトレイでやり取りを行ってください．`20UPG...`から始まるATLASシリアル番号は複雑なので，REPIC側とのやり取りでは使わない．

!!! note
    PCBに書いてある山下IDとATLASシリアル番号の対応は[spreadsheet](https://docs.google.com/spreadsheets/d/1BrP1ZUhfLx81iy4winwhAItocJb6Eg7z8EUW_kOVk5g/edit?usp=sharing)の「Yamashita PCB v4.1 QC」のタブから参照する．

## 作業手順

1. ワイヤをすべて打ってもらうように依頼する

2. CoolingBox `repicdaq[1-3]`のどれかにモジュールを配置して25℃で運転する．

3. 対応するATLASシリアル番号がITkPD上に生成されていることを確認する（通常されているべき）

4. DAQ PC上にFE configとconnectivityを生成する．
   
   ```bash
   cd /nas/daq/QCtest/module
   generateYARRConfig -sn 20UPGM22110427 --outdir . \
   --accessCode1 YOUR_ACCESS_CODE1 \
   --accessCode2 YOUR_ACCESS_CODE2
   ```

5. Digital scanを実行し，e-fuseの照合を行う．
   
   ```
   ./bin/scanConsole -s configs/scans/rd53b/std_digitalscan.json \
   -c /nas/daq/QCtest/module/20UPGM22110427/20UPGM22110427_L2_warm.json \
   -r configs/controller/specCfg-rd53b-16x1.json 
   ```
   
   !!! note
   
        e-fuseとは，それぞれのFE chipに刻印されているシリアル番号に対応する情報です．YARRではFE chipのconfig (JSON)に書かれた16進数シリアル番号（`Parameter.Name`)が，FE chipのe-fuseと一致していることを要求します．つまり，もしe-fuseが照合しない場合には，production database上のFE chipの登録情報に誤りがあることを示しています．このとき，以降の読み出しテストはすべて意味をなさないので，ProductionDB上の登録情報を修正する必要があります．
   
    !!! warning
   
        e-fuseが照合しないことがわかった場合はただちにシフトリーダーに連絡してください．

7. Visual inspectionを行い，Iref trimの対応関係を __目視で__ 確認する．
　目視を行う際に，[参照図](docs/ITkPix_v3-2_BondingMap_v2023-02-15a.pdf)を参照しながら行うこと．

8. Visual InspectionのQC登録を行う．

9. Wirebond Pull TestのQC登録を行う．

## E-fuseが正しくないことが判明したとき（エキスパート向け）

正しく登録し直すためには，DB expert workが必要になります．

!!! note
    深呼吸をして落ち着いて取り組みましょう．

まず，digial scanの結果として読み取られたe-fuseの値をそれぞれのFEごとにメモしてください．念のため，connectivity JSONでenableフラグをFE一つずつenableして，確実を期すことを勧めます．

!!! warning
 e-fuseの照合が失敗するとき，YARRではいわゆる"new"と"old"という２種類のシリアル番号をデコードするアルゴリズムを試行します．この２種類のアルゴリズムは同一の結果を返すとは限らないので，e-fuseの読み出しの結果だけから一意にシリアル番号が決定できない可能性があります．

e-fuseの16進数シリアル番号候補からATLASシリアル番号`20UPGFCxxxxxxx`に変換し，そのシリアル番号の所在を確認します．場合によっては，そのシリアル番号はすでに他のBareModuleに使われている可能性もありますし，あるいはシリアル番号が存在しない可能性もあります．

* シリアル番号が存在しない場合は，正しいシリアル番号ではないので，new/oldのうちもう一つのシリアル番号が真のシリアル番号だと確定します．
* シリアル番号がすでにほかのBareModuleに使われていて，かつそのモジュールのQCが終わっている場合には，正しいシリアル番号ではないので，new/oldのうちもう一つのシリアル番号が真のシリアル番号だと確定します．逆に，モジュールのアセンブリ・QCが終わっていない場合にはそのシリアル番号は候補として残ります．

これらのチェックを経て，なおシリアル番号が確定しない場合には，シリアル番号を確定させる他の必要十分な情報が得られるまでそのモジュールについての作業は保留してください．

以下ではシリアル番号がすべて確定したと仮定します．

このとき，そのFEが他の未アセンブル・モジュールに属している場合には，そのFEをdisassembleします．FEがBareModuleに属してなく，Waferに属している，あるいは単独のコンポーネントとして存在する場合には，FEをassembleします．

同時に，Spreadsheetの該当するモジュールについて，FE chipの情報を正しく修正します．

!!! warning
    このとき，Iref trim, VDDD trim, VDDA trimの欄の再構成も忘れないようにしてください．

!!! note
    この作業を行った時点で，すでにモジュールまたはBareModuleをLocalDBの管理下に置いている場合には，LocalDBのモジュール─FEおよびBareModule─FEの対応関係が誤って記録されているので，これを更新する必要があります．この修正を行うためには，LocalDBで「Pull components from ITkPD」を行うことで，対応関係を再構築できます．
